from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class Movie(models.Model):
    """
    Defines video object for rental
    """
    title = models.CharField(max_length=120)
    is_available = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    stream_link = models.CharField(max_length=255)

    def __str__(self):
        return self.title


class Order(models.Model):
    """
    Define 'busket' for movie items
    """
    STATUSES = (
        (0, 'NEW'),
        (1, 'OPEN'),
        (2, 'APPROVED'),
        (3, 'CLOSED')
        )

    user = models.ForeignKey(User, related_name='orders')
    movie = models.ManyToManyField(Movie, through='OrderItem')
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=0)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_expired = models.DateTimeField()

    def is_expired(self):
        """
        Check order for expiration time and return status integer-value
        """
        now = timezone.now().replace(microsecond=0)
        if self.date_expired <= now:  # exp should be less than now
            self.status = 3
            return True
        return False

    def __str__(self):
        return 'Order {} of {}'.format(self.id, self.user)


class OrderItem(models.Model):
    """
    Define movie item for busket
    """
    client = models.ForeignKey(User)
    movie = models.ForeignKey(Movie, related_name='order_item')
    order = models.ForeignKey(Order, related_name='items')
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Item {} for order {}'.format(self.id, self.order.id)
