import datetime

from django.conf import settings
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status, permissions
from rest_framework.response import Response
from rest_framework.decorators import detail_route, list_route

from .models import Movie, Order, OrderItem
from .serializers import MovieSerializer, OrderSerializer, OrderItemSerializer


class MovieViewSet(viewsets.ModelViewSet):
    """
    Add movie to order and set expiration
    """
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    permission_classes = (permissions.AllowAny,)

    def get_queryset(self):
        """
        Filter movies by title
        Example: http://localhost:8000/movies/?filter=titl
        """
        keyword = self.request.query_params.get('filter', None)
        if keyword is not None:
            self.queryset = self.queryset.filter(title__icontains=keyword)
        return self.queryset

    @detail_route(methods=['post'],
                  permission_classes=[permissions.IsAuthenticated])
    def add(self, request, pk=None):
        movie = get_object_or_404(Movie, pk=pk)
        if movie:
            if request.user.orders.filter(status=1).count() == 0:
                # Create new order if there are no OPEN yet
                now = datetime.datetime.now()
                exp_date = datetime.timedelta(minutes=settings.RENTAL_MINUTES)
                timedelta = now + exp_date
                order = Order.objects.create(user=request.user, status=1,
                                             date_expired=timedelta)
                item = OrderItem.objects.create(client=request.user,
                                                movie=movie, order=order)
                order.items.add(item)
                order.save()
            elif request.user.orders.filter(status=1).count() == 1:
                # Add movie to exist OPEN order
                order = request.user.orders.filter(status=1).first()
                item = OrderItem.objects.create(client=request.user,
                                                movie=movie, order=order)
                order.items.add(item)
                order.save()
            else:
                # If there are more than one OPEN orders
                return Response(
                    {'details': 'more than one OPEN orders are exist'},
                    status=HTTP_400_BAD_REQUEST
                    )

            serialized = OrderSerializer(order, context={'request': request})
            return Response(serialized.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)


class OrderViewSet(viewsets.ModelViewSet):
    """
    Return orders of requested user only.
    Submit only OPEN order
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        self.queryset = Order.objects.filter(user=self.request.user)
        return self.queryset

    @list_route(methods=['post'], url_path='checkout')
    def submit_order(self, request, pk=None):

        if request.user.orders.filter(status=1).count() == 1:
            user_order = Order.objects.get(user=request.user, status=1)
            user_order.status = 2  # "APPROVED"
            user_order.save()
            serialized = OrderSerializer(user_order, context={'request': request})
            return Response(serialized.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_404_NOT_FOUND)


class OrderItemViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Return the list of all items only for requested user
    """
    queryset = OrderItem.objects.all()
    serializer_class = OrderItemSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def list(self, request):
        query_data = OrderItem.objects.filter(client=request.user)
        serialized = OrderItemSerializer(query_data, context={'request': request}, many=True)
        return Response(serialized.data, status=status.HTTP_200_OK)
