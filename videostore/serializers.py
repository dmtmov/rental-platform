from django.conf import settings
from django.utils import timezone
from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Movie, Order, OrderItem
from users.serializers import UserSerializer


class MovieSerializer(serializers.ModelSerializer):
    """
    Return stream link if user is authorized and checked out an order
    """
    stream_link = serializers.SerializerMethodField('get_link')

    class Meta:
        model = Movie
        fields = ('title', 'stream_link')

    def get_link(self, obj):
        user = self.context['request'].user

        if user.is_authenticated() and user.orders.filter(status=2).count() == 1:
            now = timezone.now().replace(microsecond=0)
            order = user.orders.get(status=2)
            items = obj.order_item.filter(client=user)

            # make link available for requested user
            if not order.is_expired() and obj in order.movie.all() \
                    and items.count() !=0:
                obj.is_available = True
                return obj.stream_link

            # mark order as CLOSED
            if order.is_expired():
                order.status = 3
                order.save()

        return '__not_available__'


class OrderSerializer(serializers.ModelSerializer):
    """
    User may have only one order with AUTHORISED status
    """
    user = UserSerializer(read_only=True)
    items = serializers.StringRelatedField(many=True)
    status = serializers.SerializerMethodField('status_name', read_only=True)
    date_created = serializers.DateTimeField(format=settings.DATETIME_FORMAT)
    date_expired = serializers.DateTimeField(format=settings.DATETIME_FORMAT)

    class Meta:
        model = Order
        fields = ('user', 'status', 'items', 'date_created', 'date_expired')

    def status_name(self, obj):
        return dict(obj.STATUSES).get(obj.status)


class OrderItemSerializer(serializers.ModelSerializer):
    """
    Define list of all user's items in busket
    """
    movie = MovieSerializer()
    order = OrderSerializer()
    client = UserSerializer()

    class Meta:
        model = OrderItem
        fields = ('client', 'movie', 'order')

