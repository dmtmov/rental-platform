from django.contrib import admin
from . import models


@admin.register(models.Movie)
class Movie(admin.ModelAdmin):
    list_display = ('title', 'is_available', 'date_created', 'date_modified')


@admin.register(models.Order)
class Order(admin.ModelAdmin):
    list_display = ('user', 'status', 'date_created', 'date_modified', 'date_expired')

    # def get_movies(self, obj):
    #     return "\n".join([p.title for p in obj.movie.all()])


@admin.register(models.OrderItem)
class OrderItem(admin.ModelAdmin):
    list_display = ('client', 'movie', 'order', 'date_created', 'date_modified')
