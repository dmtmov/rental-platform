from django.conf.urls import url, include
from django.contrib import admin

from rest_framework.authtoken import views as rest_views
from rest_framework import routers

from videostore import views
from users import views as users_views

"""
Platform routing

/users/             [GET]   (IsAuthenticated)   # all users
/users/             [POST]  (AllowAny)          # registration
/users/login/       [POST]  (AllowAny)          # login
/users/<pk>/        [GET]   (IsAuthenticated)   # user details

/movies/            [GET]   (AllowAny)          # all movies 
/movies/?filter=    [GET]   (AllowAny)          # filter movies by title
/movies/<pk>/       [GET]   (AllowAny)          # movie details
/movies/<pk>/want/  [POST]  (IsAuthenticated)   # add movie to a busket

/orders/            [POST]  (IsAuthenticated)   # create order (! see only yours)
/orders/            [GET]   (IsAuthenticated)   # user's orders (! see only yours)
/orders/<pk>        [GET]   (IsAuthenticated)   # user's order details (! see only yours)
/items/             [GET]   (IsAuthenticated)   # user's items (! see only yours)
/items/<pk>         [GET]   (IsAuthenticated)   # user's item details (! see only yours)

"""

router = routers.SimpleRouter()
router.register(r'users', users_views.UserViewSet)
router.register(r'orders', views.OrderViewSet)
router.register(r'items', views.OrderItemViewSet)
router.register(r'movies', views.MovieViewSet)

urlpatterns = [
    url(r'^/?', admin.site.urls),
    url(r'^users/login/?$', rest_views.obtain_auth_token),
] + router.urls
