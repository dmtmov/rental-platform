## movie rental platform

### installation

```bash 
git clone https://bitbucket.org/mdvua/rental-platform.git
cd rental-platform
docker-compose build
docker-compose up -d
```
creadentials for admin panel

```bash
url: 0.0.0.0:8000
login: admin
pass: admin
```

platform settings

```bash
RENTAL_MINUTES = 15
DATETIME_FORMAT = '%Y-%m-%d %H:%M'
```

### usage
1.
Create user

```bash
$ curl -X POST http://localhost:8000/users/ -d '{"email": "email@test.com", "username": "testuser", "password": "testpassword"}' -H 'Content-Type: application/json'

{"username":"testuser","token":"4c77ebc58a8ed733a48e0f26ad15543009e70bbc"}
```

2.
Login

```bash
$ curl -X POST http://localhost:8000/users/login/ -d '{"username": "testuser", "password": "testpassword"}' -H 'Content-Type: application/json'

{"token":"4c77ebc58a8ed733a48e0f26ad15543009e70bbc"}
```

3.
Check all available movies. You may use "**?filter=title**" in url to find out by title

```bash
$ curl -X GET http://localhost:8000/movies/\?filter\=title1 -H 'Authorization: Token 4c77ebc58a8ed733a48e0f26ad15543009e70bbc'

{"count":2,"next":null,"previous":null,"results":[{"title":"Title1","stream_link":"__not_available__"},
{"title":"Title10","stream_link":"__not_available__"}]}
```

4.
Add selected movie to busket

```bash
$ curl -X POST http://localhost:8000/movies/1/add/ -H 'Authorization: Token 4c77ebc58a8ed733a48e0f26ad15543009e70bbc'

{"user":{"username":"testuser"},"status":"OPEN","items":["Item 38 for order 18"],"date_created":"2017-06-11 13:02","date_expired":"2017-06-11 13:17"}
```

5.
Check all orders and proceed to checkout already created (I added two movies in example)

```bash
$ curl -X POST http://localhost:8000/orders/checkout/ -H 'Authorization: Token 4c77ebc58a8ed733a48e0f26ad15543009e70bbc'

{"user":{"username":"testuser"},"status":"APPROVED","items":["Item 38 for order 18","Item 39 for order 18"],"date_created":"2017-06-11 13:02","date_expired":"2017-06-11 13:17"}
```

6.
Check movies for available stream-link. They gonna be available by time-range from parameter RENTAL_MINUTES from settings

```bash
$ curl -X GET http://localhost:8000/movies/ -H 'Authorization: Token 4c77ebc58a8ed733a48e0f26ad15543009e70bbc'

{"count":8,"next":null,"previous":null,"results":[{"title":"Title1","stream_link":"________"},{"title":"Title2","stream_link":"__not_available__"},{"title":"Title3","stream_link":"________"},{"title":"Title4","stream_link":"__not_available__"},{"title":"Title5","stream_link":"__not_available__"},{"title":"Title6","stream_link":"__not_available__"},{"title":"Title7","stream_link":"__not_available__"},
{"title":"Title8","stream_link":"__not_available__"},
{"title":"Title9","stream_link":"__not_available__"},{"title":"Title10","stream_link":"__not_available__"}]}
```
