from django.contrib.auth.models import User
from rest_framework import viewsets, status, permissions, authentication
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.decorators import detail_route, list_route

from users.serializers import UserSerializer
from videostore.serializers import OrderSerializer
from videostore.models import Movie, Order


class UserViewSet(viewsets.ModelViewSet):
    """
    Create new user with token
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.AllowAny,)

    def create(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            # create token
            if user:
                token = Token.objects.create(user=user)
                response = serializer.data
                response['token'] = token.key
                return Response(response, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
