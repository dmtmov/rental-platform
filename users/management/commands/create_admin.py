from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Create admin user'

    username = 'admin'

    def handle(self, *args, **kwargs):
        User = get_user_model()
        if not User.objects.filter(username=self.username).exists():
            User.objects.create_superuser(self.username, '', self.username)
