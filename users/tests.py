from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from rest_framework.authtoken.models import Token

from pprint import pprint

"""
Test cases:

create user - no username
create user - username already exist
create user - short password (8 chars min)
create user - no password
create user - email exist
create user - invalid email (name@domain.com)
create user - no email

"""


class UsersTest(APITestCase):

    def setUp(self):
        self.user = User.objects.create_user('testuser',
                                             'test@example.com',
                                             'testpassword')
        self.token = Token.objects.create(user=self.user)
        self.create_url = reverse('user-list')

    def test_create_user(self):
        """
        # TODO: add token creation check
        """
        data = {
            'username': 'foobar',
            'email': 'foobar@example.com',
            'password': 'somepassword'
        }
        response = self.client.post(self.create_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # do not return pasword and email fields
        self.assertEqual(response.data['username'], data['username'])
        self.assertFalse('password' in response.data and
                         'email' in response.data)


