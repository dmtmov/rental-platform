from django.contrib.auth.models import User

from rest_framework import serializers
from rest_framework.validators import UniqueValidator


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serialize User fields. Validate for unique email and username values
    """
    email = serializers.EmailField(max_length=32, validators=[
        UniqueValidator(queryset=User.objects.all())], write_only=True)
    username = serializers.CharField(max_length=20, validators=[
        UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(min_length=6, write_only=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password')

    def create(self, validated_data):
        user = User.objects.create_user(
            validated_data['username'],
            validated_data['email'],
            validated_data['password'],
            )
        return user
